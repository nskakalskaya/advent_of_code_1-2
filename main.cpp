#include <iostream>
#include <string>
#include <sstream>
#include <fstream>


int main()
{
    std::string inputFilename{"/home/qxz0abk/advent_of_code/advent_of_code_2/input_2.txt"};
    /*std::cout << "Enter file name: " << std::endl;
    std::cin >> inputFilename;
     */
    std::ifstream inputStream;
    std::string line;
    inputStream.exceptions(std::ifstream::badbit);
    try {
        inputStream.open(inputFilename);
        long totalSum = 0;
        while (std::getline(inputStream, line)) {
            std::istringstream iss(line);
            long mass;
            if (!(iss >> mass)) {
                break;
            }
            auto countSum = [](auto &&self, long mass) -> long { return (mass <=0) ? 0 : self(self, (mass / 3) - 2) + mass; };
            totalSum += countSum(countSum, (mass / 3) - 2);
        }
        std::cout << totalSum << std::endl;
    }
    catch (const std::ifstream::failure &e) {
        std::cout << "Caught exception " << e.what() << std::endl;
        return -1;
    }
    inputStream.close();
    return 0;
}

